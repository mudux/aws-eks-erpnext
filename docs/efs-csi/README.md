### Download policy config

```shell
curl -o iam-policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-efs-csi-driver/master/docs/iam-policy-example.json
```

### Create Policy

```shell
aws iam create-policy \
  --policy-name AmazonEKS-EFS-CSI-Driver-Policy-eks-erpnext \
  --policy-document file://iam-policy.json
```

### Create IAM Service Account

```
eksctl create iamserviceaccount \
  --name efs-csi-controller-sa \
  --namespace kube-system \
  --cluster eks-erpnext \
  --attach-policy-arn arn:aws:iam::123456789012:policy/AmazonEKS-EFS-CSI-Driver-Policy-eks-erpnext \
  --approve \
  --override-existing-serviceaccounts \
  --region us-east-1
```

### Create FS

```shell
export vpc_id=$(aws eks describe-cluster --region us-east-1 \
  --name eks-erpnext \
  --query "cluster.resourcesVpcConfig.vpcId" \
  --output text)

# vpc_id=vpc-0473d3d90a2630f3c

export cidr_range=$(aws ec2 describe-vpcs --region us-east-1 \
  --vpc-ids $vpc_id \
  --query "Vpcs[].CidrBlock" \
  --output text)

# cidr_range=192.168.0.0/16

export security_group_id=$(aws ec2 create-security-group \
    --group-name EFSERPNextSecurityGroup \
    --description "EFS ERPNext security group" \
    --vpc-id $vpc_id \
    --output text)

aws ec2 authorize-security-group-ingress --region us-east-1 \
  --group-id $security_group_id \
  --protocol tcp \
  --port 2049 \
  --cidr $cidr_range

# output begin
{
    "Return": true,
    "SecurityGroupRules": [
        {
            "SecurityGroupRuleId": "sgr-05ff3d87b9a754344",
            "GroupId": "sg-0abc63798f226756f",
            "GroupOwnerId": "123456789012",
            "IsEgress": false,
            "IpProtocol": "tcp",
            "FromPort": 2049,
            "ToPort": 2049,
            "CidrIpv4": "192.168.0.0/16"
        }
    ]
}
# output end

export file_system_id=$(aws efs create-file-system --region us-east-1 \
  --performance-mode generalPurpose \
  --query 'FileSystemId' \
  --output text)

# file_system_id=fs-0c27b6b8c216929ff

kubectl get nodes

NAME                             STATUS   ROLES    AGE     VERSION
ip-192-168-2-115.ec2.internal    Ready    <none>   5h51m   v1.21.14-eks-48e63af
ip-192-168-50-129.ec2.internal   Ready    <none>   5h51m   v1.21.14-eks-48e63af

aws ec2 describe-subnets --region us-east-1 \
  --filters "Name=vpc-id,Values=$vpc_id" \
  --query 'Subnets[*].{SubnetId: SubnetId,AvailabilityZone: AvailabilityZone,CidrBlock: CidrBlock}' \
  --output table

---------------------------------------------------------------------
|                          DescribeSubnets                          |
+------------------+-------------------+----------------------------+
| AvailabilityZone |     CidrBlock     |         SubnetId           |
+------------------+-------------------+----------------------------+
|  us-east-1d      |  192.168.64.0/19  |  subnet-02cf787fbf87735d8  |
|  us-east-1d      |  192.168.0.0/19   |  subnet-046f311ab676b1ebe  |
|  us-east-1a      |  192.168.96.0/19  |  subnet-07f6039b60b835a2c  |
|  us-east-1a      |  192.168.32.0/19  |  subnet-0c2a3fa6ee487f1f8  |
+------------------+-------------------+----------------------------+

aws efs create-mount-target --region us-east-1 \
  --file-system-id $file_system_id \
  --subnet-id subnet-02cf787fbf87735d8 \
  --security-groups $security_group_id

aws efs create-mount-target --region us-east-1 \
  --file-system-id $file_system_id \
  --subnet-id subnet-046f311ab676b1ebe \
  --security-groups $security_group_id

aws efs create-mount-target --region us-east-1 \
  --file-system-id $file_system_id \
  --subnet-id subnet-07f6039b60b835a2c \
  --security-groups $security_group_id

aws efs create-mount-target --region us-east-1 \
  --file-system-id $file_system_id \
  --subnet-id subnet-0c2a3fa6ee487f1f8 \
  --security-groups $security_group_id
```

### Install EFS CSI and Storage class

Note: You can use FluxCD yaml(s) (`infrastructure/aws-efs-csi-driver/release.yaml` and `infrastructure/aws-efs-csi-driver/storageclass.yaml`) from this repo instead of manually creating releases and resources.

```shell
helm repo add aws-efs-csi-driver https://kubernetes-sigs.github.io/aws-efs-csi-driver
helm repo update
helm upgrade -i aws-efs-csi-driver aws-efs-csi-driver/aws-efs-csi-driver \
  --namespace kube-system \
  --version 2.2.6 \
  --set image.repository=602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/aws-efs-csi-driver \
  --set controller.serviceAccount.create=false \
  --set controller.serviceAccount.name=efs-csi-controller-sa
```

Note: Get image address as per region from here https://docs.aws.amazon.com/eks/latest/userguide/add-ons-images.html

Install Storage Class

Change `parameters.fileSystemId` from `infrastructure/aws-efs-csi-driver/storageclass.yaml` and apply.

```
kubectl apply -f infrastructure/aws-efs-csi-driver/storageclass.yaml
```
