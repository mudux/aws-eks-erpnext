### Install Helm Chart

Note:
- You can use FluxCD yaml(s) from this repo instead of manually creating releases and resources.
- Create sites by creating jobs manually with helm command. Or generate templates and place in gitops.
- Use your rds endpoint.
- make changes to `apps/erpnext/release.yaml` and `apps/erpnext/configure-kbi-job.yaml`.
- make changes to `apps/erpnext/kbi-ingress.yaml`.

### Configure bench

```shell
kubectl -n erpnext apply -f apps/erpnext/configure-kbi-job.yaml
```

### Add ingress

```shell
kubectl -n erpnext apply -f apps/erpnext/kbi-ingress.yaml
```
